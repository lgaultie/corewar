/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   common.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sregnard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/27 20:14:35 by sregnard          #+#    #+#             */
/*   Updated: 2019/10/27 20:26:56 by sregnard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

void cc(void)
{
	write(1, "common\n", 7);
}
